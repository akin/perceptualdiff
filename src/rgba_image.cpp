/*
RGBAImage.cpp
Copyright (C) 2006-2011 Yangli Hector Yee
Copyright (C) 2011-2016 Steven Myint, Jeff Terrace

(This entire file was rewritten by Jim Tilander)

This program is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation; either version 2 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program; if not, write to the Free Software Foundation, Inc., 59 Temple
Place, Suite 330, Boston, MA 02111-1307 USA
*/

#include "rgba_image.h"

#include <assert.h>
#include <cstring>
#include <string>
#include <array>

#include <algorithm>

#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"
#define STB_IMAGE_WRITE_IMPLEMENTATION
#include "stb_image_write.h"


namespace pdiff
{
    namespace 
    {
        // Sampling code from https://blog.demofox.org/2015/08/15/resizing-images-with-bicubic-interpolation/
        #define CLAMP(v, min, max) if (v < min) { v = min; } else if (v > max) { v = max; } 

        // t is a value that goes from 0 to 1 to interpolate in a C1 continuous way across uniformly sampled data points.
        // when t is 0, this will return B.  When t is 1, this will return C.  Inbetween values will return an interpolation
        // between B and C.  A and B are used to calculate slopes at the edges.
        float cubicHermite (float A, float B, float C, float D, float t)
        {
            float a = -A / 2.0f + (3.0f*B) / 2.0f - (3.0f*C) / 2.0f + D / 2.0f;
            float b = A - (5.0f*B) / 2.0f + 2.0f*C - D / 2.0f;
            float c = -A / 2.0f + C / 2.0f;
            float d = B;
        
            return a*t*t*t + b*t*t + c*t + d;
        }

        float lerp(float A, float B, float t)
        {
            return A * (1.0f - t) + B * t;
        }
        
        const uint8_t* getPixelClamped(const uint8_t *data, int width, int height, int x, int y)
        {
            CLAMP(x, 0, width - 1);
            CLAMP(y, 0, height - 1);    
            return &data[(y * width) + x * 4];
        }
        
        void sampleNearest(const uint8_t *data, uint8_t* dst, int width, int height, float u, float v)
        {
            // calculate coordinates
            int xint = int(u * width);
            int yint = int(v * height);
        
            auto pixel = getPixelClamped(data, width, height, xint, yint);
            for (int i = 0; i < 4; ++i)
            {
                dst[i] = pixel[i];
            }
        }
        
        void sampleLinear(const uint8_t *data, uint8_t* dst, int width, int height, float u, float v)
        {
            // calculate coordinates -> also need to offset by half a pixel to keep image from shifting down and left half a pixel
            float x = (u * width) - 0.5f;
            int xint = int(x);
            float xfract = x - floor(x);
        
            float y = (v * height) - 0.5f;
            int yint = int(y);
            float yfract = y - floor(y);
        
            // get pixels
            auto p00 = getPixelClamped(data, width, height, xint + 0, yint + 0);
            auto p10 = getPixelClamped(data, width, height, xint + 1, yint + 0);
            auto p01 = getPixelClamped(data, width, height, xint + 0, yint + 1);
            auto p11 = getPixelClamped(data, width, height, xint + 1, yint + 1);
        
            // interpolate bi-linearly!
            for (int i = 0; i < 4; ++i)
            {
                float col0 = lerp(p00[i], p10[i], xfract);
                float col1 = lerp(p01[i], p11[i], xfract);
                float value = lerp(col0, col1, yfract);
                CLAMP(value, 0.0f, 255.0f);
                dst[i] = uint8_t(value);
            }
        }
        
        void sampleBicubic(const uint8_t *data, uint8_t* dst, int width, int height, float u, float v)
        {
            // calculate coordinates -> also need to offset by half a pixel to keep image from shifting down and left half a pixel
            float x = (u * width) - 0.5f;
            int xint = int(x);
            float xfract = x - floor(x);
        
            float y = (v * height) - 0.5f;
            int yint = int(y);
            float yfract = y - floor(y);
        
            // 1st row
            auto p00 = getPixelClamped(data, width, height, xint - 1, yint - 1);
            auto p10 = getPixelClamped(data, width, height, xint + 0, yint - 1);
            auto p20 = getPixelClamped(data, width, height, xint + 1, yint - 1);
            auto p30 = getPixelClamped(data, width, height, xint + 2, yint - 1);
        
            // 2nd row
            auto p01 = getPixelClamped(data, width, height, xint - 1, yint + 0);
            auto p11 = getPixelClamped(data, width, height, xint + 0, yint + 0);
            auto p21 = getPixelClamped(data, width, height, xint + 1, yint + 0);
            auto p31 = getPixelClamped(data, width, height, xint + 2, yint + 0);
        
            // 3rd row
            auto p02 = getPixelClamped(data, width, height, xint - 1, yint + 1);
            auto p12 = getPixelClamped(data, width, height, xint + 0, yint + 1);
            auto p22 = getPixelClamped(data, width, height, xint + 1, yint + 1);
            auto p32 = getPixelClamped(data, width, height, xint + 2, yint + 1);
        
            // 4th row
            auto p03 = getPixelClamped(data, width, height, xint - 1, yint + 2);
            auto p13 = getPixelClamped(data, width, height, xint + 0, yint + 2);
            auto p23 = getPixelClamped(data, width, height, xint + 1, yint + 2);
            auto p33 = getPixelClamped(data, width, height, xint + 2, yint + 2);
        
            // interpolate bi-cubically!
            // Clamp the values since the curve can put the value below 0 or above 255
            for (int i = 0; i < 4 ; ++i)
            {
                float col0 = cubicHermite(p00[i], p10[i], p20[i], p30[i], xfract);
                float col1 = cubicHermite(p01[i], p11[i], p21[i], p31[i], xfract);
                float col2 = cubicHermite(p02[i], p12[i], p22[i], p32[i], xfract);
                float col3 = cubicHermite(p03[i], p13[i], p23[i], p33[i], xfract);
                float value = cubicHermite(col0, col1, col2, col3, yfract);
                CLAMP(value, 0.0f, 255.0f);
                dst[i] = uint8_t(value);
            }
        }
    }
    
    std::shared_ptr<RGBAImage> RGBAImage::down_sample(unsigned int w, unsigned int h) const
    {
        if (w == 0)
        {
            w = width_ / 2;
        }

        if (h == 0)
        {
            h = height_ / 2;
        }

        if (width_ <= 1 || height_ <= 1)
        {
            return nullptr;
        }
        if (width_ == w && height_ == h)
        {
            return nullptr;
        }
        assert(w <= width_);
        assert(h <= height_);

        std::shared_ptr<RGBAImage> image = std::make_shared<RGBAImage>(w, h, name_);

        const uint8_t *src = static_cast<const uint8_t*>(static_cast<const void*>(this->get_data()));
        uint8_t *dst = static_cast<uint8_t*>(static_cast<void*>(image->get_data()));
        
        float xRatio = (float)width_ / w;
        float yRatio =  (float)height_ / h;
        
        for(size_t y = 0 ; y < h; y++)
        {
            for(size_t x = 0 ; x < w; x++)
            {
                size_t index = y * w + x;

                sampleBicubic(src, dst + (index * 4), width_, height_, x * xRatio, y * yRatio);
            }
        }

        return image;
    }

    void RGBAImage::write_to_file(const std::string& filename) const
    {
        std::string ending;
        if(filename.size() > 3)
        {
            ending = filename.substr(filename.size() - 3);
            std::transform(ending.begin(), ending.end(), ending.begin(), ::tolower);
        }

		int result = -1;
        if (ending == "png")
        {
            int stride_in_bytes = 0;
            result = stbi_write_png(filename.c_str(), width_, height_, 4, data_.data(), stride_in_bytes);
        }
        else if (ending == "bmp")
        {
            result = stbi_write_bmp(filename.c_str(), width_, height_, 4, data_.data());
        }
        else if (ending == "tga")
        {
            result = stbi_write_tga(filename.c_str(), width_, height_, 4, data_.data());
        }
        else if (ending == "jpg")
        {
            int quality = 90;
            result = stbi_write_jpg(filename.c_str(), width_, height_, 4, data_.data(), quality);
        }
        else
        {
            throw RGBImageException("Can't save to unknown filetype '" + filename + "'");
        }

        if (result != 0)
        {
            throw RGBImageException("Failed to save to '" + filename + "'");
        }
    }

    std::shared_ptr<RGBAImage> read_from_file(const std::string &filename)
    {
        std::string ending;
        if(filename.size() > 3)
        {
            ending = filename.substr(filename.size() - 3);
            std::transform(ending.begin(), ending.end(), ending.begin(), ::tolower);
        }

        std::shared_ptr<RGBAImage> image;

		bool detected = false;

        // try to save the day, and read whatever is there
        if (!detected)
        {
            int width;
            int height;
            int comp;
            stbi_uc *result = stbi_load(filename.c_str(), &width, &height, &comp, 4);
            if(result == nullptr)
            {
                throw RGBImageException("Failed to load the image " + filename);
            }

            image = std::make_shared<RGBAImage>(static_cast<unsigned int>(width), static_cast<unsigned int>(height), filename);
            memcpy(image->get_data(), result, width * height * 4);
            stbi_image_free(result);
        }
        //{
        //    throw RGBImageException("Unknown filetype '" + filename + "'");
        //}

        return image;
    }
}
